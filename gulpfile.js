const path = require('path');
const gulp = require('gulp');
const concat = require('gulp-concat');

gulp.task('default', function () {
    const vendors = [
        './libs/three.min.js',
        // './libs/OrbitControls.js',
        './libs/TransformControls.js',
        './libs/chpok.js',
        './libs/STLLoader.js',
        './libs/Detector.js',
        './libs/TrackballControls.js'
    ];

    return gulp.src(vendors)
        .pipe(concat('vendor.js'))
        .pipe(gulp.dest(path.join(__dirname, 'web/dist')))
});