var THREE = THREE || {};

THREE.Chpok = (function(){

    /**
     *
     * @param container
     * @param camera
     * @param intersects
     * @constructor
     */
  function Chpok( container, camera, intersects ){

    var Chpok = {
      Mouse: {
        current: new THREE.Vector2(),
        previous: new THREE.Vector2(),
        gap: new THREE.Vector2(),
        down: false
      },
      raycaster: new THREE.Raycaster(),
      camera: camera,
      container: container
    };

    Chpok.Intersects = intersects || [];

    // EVENTS
    Chpok.Events = {

      _: {
        dragged: false,
        active: false,
        hover: false
      },

      EventSwitcher: function( event ){

      },

      MouseDown:  function (event) {
          var _inter = Chpok.Events.INTER(event);
          if( _inter ){
            this.interDown = _inter;
            _inter.event = event;
            Chpok.Events._.active = _inter;
            _inter.object.dispatchEvent({ type: 'mousedown', inter: _inter });
        };
        Chpok.Mouse.down = true;
      },

      MouseUp: function(event){
        var _inter = Chpok.Events.INTER(event);
          if( _inter ){
              _inter.event = event;
          if(this.interDown && (_inter.object.uuid == this.interDown.object.uuid) && (_inter.event.clientX == this.interDown.event.clientX && _inter.event.clientY == this.interDown.event.clientY)){
              _inter.object.dispatchEvent({ type: 'mouseclick', inter: _inter });
          }
            Chpok.Events._.active = false;
            _inter.object.dispatchEvent({ type: 'mouseup', inter: _inter });
        } else {
            this.interDown = false
        }
        Chpok.Mouse.down = false;
      },

      MouseMove: function(event){
        Chpok.Events.MouseResetPosition(event);

        var _inter = Chpok.Events.INTER(event);

        if( _inter ){

          _inter.event = event;

          if( Chpok.Events._.hover ){
            if( _inter.object != Chpok.Events._.hover.object ){
              Chpok.Events._.hover.object.dispatchEvent({ type: 'mouseleave', inter: _inter });
              Chpok.Events._.hover = _inter;
              Chpok.Events._.hover.object.dispatchEvent({ type: 'mouseenter', inter: _inter });
              Chpok.Events._.hover.object.dispatchEvent({ type: 'mousemove', inter: _inter });
            }
          } else {
            Chpok.Events._.hover = _inter;
            Chpok.Events._.hover.object.dispatchEvent({ type: 'mouseenter', inter: _inter });
            Chpok.Events._.hover.object.dispatchEvent({ type: 'mousemove', inter: _inter });
          }
        } else {
          if(Chpok.Events._.hover){
            Chpok.Events._.hover.event = event;
            Chpok.Events._.hover.object.dispatchEvent({ type: 'mouseleave', inter: _inter });
            Chpok.Events._.hover = false;
          };
        };

      },

      MouseResetPosition: function(eve){
        Chpok.Mouse.previous.copy( Chpok.Mouse.current );
        Chpok.Mouse.current.set((((eve.clientX - Chpok.container.offsetLeft) / Chpok.container.offsetWidth) * 2) - 1, -(((eve.clientY - Chpok.container.offsetTop) / Chpok.container.offsetHeight) * 2) + 1);
        Chpok.Mouse.gap.set( Chpok.Mouse.current.x - Chpok.Mouse.previous.x,  Chpok.Mouse.current.y - Chpok.Mouse.previous.y);
      },

      INTER: function( event ){
        Chpok.raycaster.setFromCamera( Chpok.Mouse.current, Chpok.camera );
        var Intersects = Chpok.raycaster.intersectObjects( Chpok.Intersects , false );
        return Intersects[0];
      },

      Context: function( event ){
        event.stopPropagation();
      },

      Listen: function () {
        Chpok.container.addEventListener( "mouseup", Chpok.Events.MouseUp, false);
        Chpok.container.addEventListener( "mousedown",  Chpok.Events.MouseDown, false);
        Chpok.container.addEventListener( "mousemove",  Chpok.Events.MouseMove, false);
        Chpok.container.addEventListener( 'contextmenu', Chpok.Events.Context, false );
      },

      Unlisten: function(){
        Chpok.container.removeEventListener( "mouseup", Chpok.Events.MouseUp, false);
        Chpok.container.removeEventListener( "mousedown",  Chpok.Events.MouseDown, false);
        Chpok.container.removeEventListener( "mousemove",  Chpok.Events.MouseMove, false);
        Chpok.container.removeEventListener( 'contextmenu', Chpok.Events.Context, false );
      }

    };

    this.setIntersects = function( arr ){
        Chpok.Intersects = ( arr instanceof Array ) ? arr : [];
    }

    Chpok.Events.Listen();

  }

  Chpok.storage = [];

  return Chpok;
})();