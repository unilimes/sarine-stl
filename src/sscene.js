export class SScene {
    constructor(meshes) {
        this.meshes = meshes;
        this.autoRotate = true;
    }

    init(root, speed, autoStop, height, width, bool) {
        this.responsive = bool;
        this.height = height;
        this.width = width;
        this.autoStop = autoStop;
        this.rot = speed * Math.PI / 720;
        this.container = document.getElementById(root);
        this.input = document.createElement("input");
        this.input.type = "file";
        this.input.style.display = "none";
        this.container.appendChild(this.input);
        this.renderer = new THREE.WebGLRenderer({antialias: true, alpha: true});
        this.renderer.setPixelRatio(window.devicePixelRatio);
        this.renderer.setSize(this.width, this.height);
        this.renderer.gammaInput = true;
        this.renderer.gammaOutput = true;
        this.renderer.shadowMap.enabled = true;
        this.renderer.shadowMap.renderReverseSided = false;
        this.container.appendChild(this.renderer.domElement);
        this.camera = new THREE.PerspectiveCamera(90, this.width / this.height, 0.1, 100);
        this.camera.name = 'Camera';
        this.camera.position.set(4, 1, 4);
        this.camera.lookAt(new THREE.Vector3());


        this.controls = new THREE.TrackballControls(this.camera, this.renderer.domElement);
        this.controls.name = 'Trackball';
        this.controls.rotateSpeed = 1.0;
        this.controls.zoomSpeed = 1.2;
        this.controls.panSpeed = 0.8;
        this.controls.noZoom = false;
        this.controls.noPan = false;
        this.controls.staticMoving = true;
        this.controls.dynamicDampingFactor = 0.3;
        this.controls.keys = [65, 83, 68];

        this.scene = new THREE.Scene();
        this.scene.name = 'Scene';
        // loader
        this.loader = new THREE.STLLoader();
        // Lights
        this.scene.add(new THREE.HemisphereLight(0x443333, 0x111122));
        this.addShadowedLight(1, 3, 0, 0xffffff, 1.25, 'DirectionalLight-White');
        this.addShadowedLight(0.5, 3, -0.5, 0xffaa00, 0.8, 'DirectionalLight-Orange');
        // renderer
        // controls
        // Trackball
        // Orbit
        // this.controls = new THREE.OrbitControls(this.camera, this.renderer.domElement);
        // this.controls.addEventListener('change', () => { this.render() });
        // this.controls.enableZoom = true;
        // this.controls.enableKeys  = false;
        // Raycaster
        THREE.Chpok(this.renderer.domElement, this.camera, this.meshes);
        // stats
        // this.stats = new Stats();
        // this.container.appendChild(this.stats.dom);
        // if (this.width === window.innerWidth && this.height === window.innerHeight) {
        // window.addEventListener('resize', () => {
        //     this.onWindowResize()
        // });
        if (this.responsive) this.onWindowResize();
        this.controls.addEventListener('change', this.render);
        if (this.responsive) {
            window.addEventListener('resize', () => {
                this.onWindowResize()
            });
        }


        if (this.autoStop) this.clickStop();

        this.render();

        this.animate();
    }

    addShadowedLight(x, y, z, color, intensity, name) {
        const directionalLight = new THREE.DirectionalLight(color, intensity);
        directionalLight.position.set(x, y, z);
        directionalLight.name = name;
        this.scene.add(directionalLight);
        directionalLight.castShadow = true;
        const d = 7;
        directionalLight.shadow.camera.left = -d;
        directionalLight.shadow.camera.right = d;
        directionalLight.shadow.camera.top = d;
        directionalLight.shadow.camera.bottom = -d;
        directionalLight.shadow.camera.near = 0.5;
        directionalLight.shadow.camera.far = 7;
        directionalLight.shadow.mapSize.width = 2048;
        directionalLight.shadow.mapSize.height = 2048;
        directionalLight.shadow.bias = -0.005;
    }

    fitCamera(radius){
        const hypotenuse = Math.sqrt(Math.pow(radius, 2) * 2);
        const diapason = hypotenuse - radius;
        const min = this.width > this.height ? this.height : this.width;
        const max = this.width < this.height ? this.height : this.width;

        const CONSTANTT =1.5;

        const t = this.width >= this.height ? CONSTANTT : (CONSTANTT) * Math.pow(max/min, 0.85);
        return (hypotenuse * t);

        // 1:1 = 1.5
        // 1:2 =
    }

    onWindowResize() {
        this.width = this.container.offsetWidth;
        this.height = this.container.offsetHeight;
        this.camera.aspect = this.width / this.height;
        this.camera.updateProjectionMatrix();
        this.renderer.setSize(this.width, this.height);
        this.controls.handleResize();
    }

    calculateRadius() {
        // const dist = this.camera.position.distanceTo(new THREE.Vector3());
        // this.camera.fov
    }

    clickStop() {
        const self = this;
        this.renderer.domElement.addEventListener('click', function anon(e) {
            self.autoRotate = false;
            e.currentTarget.removeEventListener(e.type, anon);
        });
    }

    startRotate(autoStop) {
        this.autoRotate = true;
        if (autoStop) this.clickStop();
    }

    animate() {

        if (this.autoRotate) {
            this.scene.rotation.y -= this.rot;
        }

        this.controls.update();
        this.render();

        requestAnimationFrame(() => {
            this.animate();
        });
    }


    render() {
        if (this.renderer) this.renderer.render(this.scene, this.camera);
    }
}