import {SScene} from './sscene';
import {SObject} from "./sobject";

/** Class representing an API. */
export class SApi {

    /**
     * Create an API example.
     */
    constructor(height = false, width = false) {
        const self = this;
        this.onloadCallback = function () {
        };
        this.meshes = [];
        this.objct = [];
        /**
         * @public API.
         */
        return this.api = {
            /**
             * @public objects[].
             */
            objects: [],
            init: (root, speed, autoStop = true) => {
                const height_ = height ? height : root.offsetHeight;
                const width_ = width ? width : (height ? height : root.offsetWidth);
                const bool = !(height);
                self.init(root, speed, autoStop, height_, width_, bool);
            },
            uploadFromDesktop: (callback) => {
                self.view.input.click();
                self.onloadCallback = (callback instanceof Function) ? callback : function () {
                };
            },
            upload: (url, callback) => {
                self.upload(url, callback);
            },
            set speed(value) {
                self.view.rot = value * Math.PI / 720;
            },
            camera: {
                fitToMeshes() {
                    const sphere = new THREE.Sphere();
                    let pointsArray = [];
                    for (let i of self.objct) {
                        pointsArray.push(i.mesh.position.clone().multiply(i.mesh.scale));
                    }
                    sphere.setFromPoints(pointsArray);
                    // const geoSph = new THREE.SphereGeometry(sphere.radius + 1.1, 100, 100);
                    // const matSph = new THREE.MeshBasicMaterial({color: 0xffff00, wireframe: true});
                    // const tempSph = new THREE.Mesh(geoSph, matSph);
                    // tempSph.position.copy(sphere.center);
                    // self.view.scene.add(tempSph);
                    self.view.controls.target.copy(sphere.center);
                    let dist = -self.view.fitCamera(sphere.radius);
                    let newcameraposition = self.view.controls.target.clone().sub(self.view.camera.position.clone()).normalize().multiplyScalar(dist).add(self.view.controls.target.clone());
                    self.view.camera.position.copy(newcameraposition);
                    self.view.controls.update();
                },
                set distance(value) {
                    let newcameraposition = self.view.controls.target.clone().sub(self.view.camera.position.clone()).normalize().multiplyScalar(value).add(self.view.controls.target.clone());
                    self.view.camera.position.copy(newcameraposition);
                    self.view.controls.update();
                }
            },
            autoRotate(autoStop = true) {
                self.view.startRotate(autoStop);
            }
        }
    }

    /**
     *
     */

    /**
     * @private onChange.
     */
    onChange() {
        this.url = URL.createObjectURL(this.view.input.files[0]);
        this.upload(this.url, this.onloadCallback);
        this.view.input.files[0].value = '';
    }

    init(root, speed, autoStop, height, width, bool) {
        this.view = new SScene(this.meshes);
        this.view.init(root, speed, autoStop, height, width, bool);
        // this.radius = this.view.calculateRadius();
        this.view.input.addEventListener('change', () => {
            this.onChange();
        });
    }

    upload(url, callback) {
        const obj = new SObject(this.view, this.meshes);
        this.objct.push(obj);
        obj.load(url, 1, (api) => {
            if (callback instanceof Function) callback(api);
            obj.myArray(this.api.objects, this.api.objects.push(api) - 1);
        });
    }
}