export class SObject {
    constructor(view, meshes) {
        this.view = view;
        this.meshes = meshes;
    }

    load(obj, radius, callback) {
        this.view.loader.load(obj, (geometry) => {
                let x, y, z;
                const material = new THREE.MeshPhongMaterial({
                    color: 16733491,
                    specular: 1118481,
                    shininess: 200,
                    transparent: true,
                    side: THREE.DoubleSide
                });
                geometry = new THREE.Geometry().fromBufferGeometry( geometry );
                geometry.computeBoundingSphere();
                geometry.verticesNeedUpdate = true;

                const scaleFactor = radius / geometry.boundingSphere.radius ;

                geometry.applyMatrix( new THREE.Matrix4().makeScale( scaleFactor, scaleFactor, scaleFactor  ) );
                this.mesh = new THREE.Mesh( geometry, material);
                this.mesh.castShadow = true;
                this.mesh.receiveShadow = true;
                this.view.scene.add(this.mesh);
                this.objId = this.meshes.push(this.mesh) - 1;

                let geo = new THREE.EdgesGeometry( geometry );
                let mat = new THREE.LineBasicMaterial( { color: 0x000000, linewidth: 2 } );
                this.edge = new THREE.LineSegments( geo, mat );
                this.edge.visible = false;
                this.mesh.add(this.edge);


                // Transform
                this.transform = new THREE.TransformControls(this.view.camera, this.view.renderer.domElement);
                this.transform.size = 0.75;
                this.transform.addEventListener('change', () => {
                    this.view.render()
                });
                this.view.scene.add(this.transform);
                // this.transform.attach(this.mesh);
                const self = this;
                this.mesh.addEventListener("mouseclick", (e) => {
                    switch (e.inter.event.button) {
                        case 0:
                            this.mesh.dispatchEvent({type: "lcb", api: self.api});
                            break;
                        case 2:
                            this.mesh.dispatchEvent({type: "rcb", api: self.api});
                            break;
                    }
                });

                this.api = {
                    position: {
                        get x() {
                            return self.mesh.position.x;
                        },
                        set x(x) {
                            self.mesh.position.x = x;
                        },
                        get y() {
                            return self.mesh.position.y;
                        },
                        set y(y) {
                            self.mesh.position.y = y;
                        },
                        get z() {
                            return self.mesh.position.z;
                        },
                        set z(z) {
                            self.mesh.position.z = z;
                        },
                        get: () => {
                            return self.mesh.position;
                        },
                        set: (x, y, z) => {
                            self.mesh.position.set(x, y, z);
                        }
                    },
                    rotation: {
                        get x() {
                            return self.mesh.rotation.x / Math.PI * 180;
                        },
                        set x(x) {
                            self.mesh.rotation.x = x * Math.PI / 180;
                        },
                        get y() {
                            return self.mesh.rotation.y / Math.PI * 180;
                        },
                        set y(y) {
                            self.mesh.rotation.y = y * Math.PI / 180;
                        },
                        get z() {
                            return self.mesh.rotation.z * Math.PI / 180;
                        },
                        set z(z) {
                            self.mesh.rotation.z = z * Math.PI / 180;
                        },
                        get: () => {
                            return {
                                x: self.mesh.rotation.x / Math.PI * 180,
                                y: self.mesh.rotation.y / Math.PI * 180,
                                z: self.mesh.rotation.z / Math.PI * 180
                            };
                        },
                        set: (x, y, z) => {
                            self.mesh.rotation.set(x * Math.PI / 180, y * Math.PI / 180, z * Math.PI / 180);
                        }
                    },
                    material: {
                        get color() {
                            return self.mesh.material.color.getHex();
                        },
                        set color(color) {
                            self.mesh.material.color.setHex(color);
                            self.edge.material.color.setHex(color);
                        },
                        colorRGB: {
                            set: (r, g, b) => {
                                self.mesh.material.color.setRGB(r, g, b);
                                self.edge.material.color.setRGB(r, g, b);
                            },
                            get: () => {
                                return self.mesh.material.color;
                            }
                        },
                        get wireframe() {
                            return self.edge.visible;
                        },
                        set wireframe(bool) {
                            self.edge.visible = Boolean(bool);
                            self.mesh.material.opacity = bool ? 0 : 1;
                        },
                        get opacity() {
                            return self.mesh.material.opacity;
                        },
                        set opacity(opacity) {
                            self.mesh.material.opacity = opacity;
                        }
                    },
                    get transform() {
                        return self.transform.visible;
                    },
                    set transform(bool) {
                        self.transform.visible = Boolean(bool);
                        self.transform.visible ? self.transform.attach(self.mesh) : self.transform.detach(self.mesh);
                    },
                    events: {
                        leftClick: {
                            on: (callback) => {
                                (self.lClick instanceof Function) ? self.api.events.leftClick.off() : false;
                                self.lClick = (callback instanceof Function) ? (e) => {
                                    callback(self.api)
                                } : function () {
                                };
                                self.mesh.addEventListener("lcb", self.lClick);
                            },
                            off: () => {
                                self.mesh.removeEventListener("lcb", self.lClick);
                            }
                        },
                        rightClick: {
                            on: (callback) => {
                                (self.rClick instanceof Function) ? self.api.events.rightClick.off() : false;
                                self.rClick = (callback instanceof Function) ? (e) => {
                                    callback(self.api)
                                } : function () {
                                };
                                self.mesh.addEventListener("rcb", self.rClick);
                            },
                            off: () => {
                                self.mesh.removeEventListener("rcb", self.rClick);
                            }
                        },
                        hover: {
                            on: (onEnter, onLeave) => {
                                (self.onEnter instanceof Function && self.onLeave instanceof Function) ? self.api.events.hover.off() : false;
                                self.onEnter = (onEnter instanceof Function) ? () => {
                                    onEnter(self.api)
                                } : function () {
                                };
                                self.onLeave = (onLeave instanceof Function) ? () => {
                                    onLeave(self.api)
                                } : function () {
                                };
                                self.mesh.addEventListener("mouseenter", self.onEnter);
                                self.mesh.addEventListener("mouseleave", self.onLeave);
                            },
                            off: () => {
                                self.mesh.removeEventListener("mouseenter", self.onEnter);
                                self.mesh.removeEventListener("mouseleave", self.onLeave);
                            }
                        }
                    },
                    delete: () => {
                        self.api.events.leftClick.off();
                        self.api.events.rightClick.off();
                        self.api.events.hover.off();
                        self.transform.detach(self.mesh);
                        self.view.scene.remove(self.transform);
                        self.view.scene.remove(self.mesh);
                        self.arr.splice(self.i, 1);
                        for (let i = self.i; i < self.arr.length; i++) {
                            self.arr[i]._updateArrayID(i);
                        }
                    },
                    _updateArrayID(i) {
                        self.i = i;
                    }
                };

                callback(this.api);
                this.geometry = geometry;
            },
            () => { // Success
                // alertify.success("Upload success");
            },
            () => {  // Error
                // alertify.error("Upload error");
            });
    }


    myArray(arr, i) {
        this.arr = arr;
        this.i = i;
    }
}