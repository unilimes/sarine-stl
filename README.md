# Sarine STL API

## Install

Clone from git,

reference it with the HTML <script> tag.
```
<head>
    <script src="dist/polyfill.js"></script>
    <script src="dist/vendor.js"></script>
    <script src="dist/bundle.js"></script>
</head>
```
## Usage

```javascript
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
    <title>Example</title>
    <script src="dist/polyfill.js"></script>
    <script src="dist/vendor.js"></script>
    <script src="dist/bundle.js"></script>
</head>
<body>
<h1 style="position: absolute;z-index: -1;">Hello world!</h1>
<div id="root"></div>
<style>
    canvas {
        border: 1px solid black
    }
</style>
<script>
    var api = new SApi();

    api.init('root', 600, 900);

    //load first STL model
    api.upload('models/incMesh.x.stl', (obj) => {

        // change position
        obj.position.y = 1;

        // change opacity
        obj.material.opacity = 1;

        // define color (default = 16733491)
        obj.material.colorRGB.set(0.3, 0.5, 0.7);

        // define right click event
        obj.events.rightClick.on((object) => {
            //remove object by right click
            object.delete();
        });

        // define right click event
        obj.events.leftClick.on((e) => {
            // enable transform controls by left click
            e.transform ? e.transform = false : e.transform = true;
        });

        // change color by hover event
        let prevCol;
        obj.events.hover.on((e) => {
            prevCol = e.material.color;
            e.material.color = (Math.random()*0xFFFFFF<<0);
        }, (e) => {
            e.material.color = prevCol;
        })
    });
    api.upload('models/incMesh2.x.stl', (obj) => {

        // change position
        obj.position.set(0.25, 1, 1);

        // change opacity
        obj.material.opacity = 0.9;

        // define color (default = 16733491)
        obj.material.colorRGB.set(0.8, 0.5, 0.2);

        // define right click event
        obj.events.rightClick.on((object) => {
            //remove object by right click
            object.delete();
        });

        // define right click event
        obj.events.leftClick.on((e) => {
            // enable transform controls by left click
            e.transform ? e.transform = false : e.transform = true;
        });

        // change color by hover event
        let prevCol;
        obj.events.hover.on((e) => {
            prevCol = e.material.color;
            e.material.color = (Math.random()*0xFFFFFF<<0);
        }, (e) => {
            e.material.color = prevCol;
        })
    });

    //load another STL model
    api.upload('models/Rough.x.stl', (obj) => {

        // change position
        obj.position.z = 1.5;

        obj.material.wireframe = true;

        // define events
        obj.events.rightClick.on((e) => {
            e.delete();
        });
        obj.events.leftClick.on((e) => {
            e.transform ? e.transform = false : e.transform = true;
        });
        let prevCol;
        obj.events.hover.on((e) => {
            prevCol = e.material.color;
            e.material.color = (Math.random()*0xFFFFFF<<0);
        }, (e) => {
            e.material.color = prevCol;
        })
    });

    //load a few more STL models
    for (let i = 0; i < 4; i++) {
        api.upload('models/polish.x.stl', (obj) => {

            // change position
            obj.position.x += i - 1.5;
            obj.position.z -= Math.random() + (i/2);
            // change position
            obj.rotation.x = Math.random()*360;
            // change opacity
            obj.material.opacity -= i / 5;
            // change color
            obj.material.color = (Math.random() * 0xFFFFFF << 0);

            obj.material.wireframe = i%2;

            //define events
            obj.events.rightClick.on((e) => {
                e.delete();
            });
            obj.events.leftClick.on((e) => {
                e.transform ? e.transform = false : e.transform = true;
            });
            let prevCol;
            obj.events.hover.on((e) => {
                prevCol = e.material.color;
                e.material.color = (Math.random()*0xFFFFFF<<0);
            }, (e) => {
                e.material.color = prevCol;
            })
        });
    }
</script>
</body>
</html>
```

see web/index.html

# API

====
- create API instance 

 `var api = new SApi()`
 
- initialize a 3D scene 

`api.init(containerID, height, width) //containerID - in this container we will render our 3D environment. `

- upload any stl file via input 

`api.uploadFromDesktop(callback) //callback(obj) obj - SApi Object instance` 

or via url 

`api.upload(url, callback) //callback(obj) obj - SApi Object instance` 



# SApi Object

=====

- `api.objects` 

return all uploaded SApi Objects

- `var obj = api.objects[i]` 

gives you access to change the SApi Object's values, `i` - Object's number

### `obj.position`

get x, y, z position 
- `obj.position.get()` 

set x, y, z position
- `obj.position.set(x, y, z)` 

Also you can change position of an object by this way:

`obj.position.x = 5`

`obj.position.y = 6`

`obj.position.z = 7`


### `obj.rotation`

get x, y, z rotation (in degree)
- `obj.rotation.get()` 

set x, y, z rotation (in degree)
- `obj.rotation.set(x, y, z)` 

Also you can change rotation of an object by this way:

`obj.rotation.x = 5` 

`obj.rotation.y = 6`

`obj.rotation.z = 7`


### `obj.material`

get color
- `obj.material.color` in HEX ("0xFFFFFF")

set color
- `obj.material.color = 0xFFFFFF` 

#### `obj.material.colorRGB`

get RGB color

- `obj.material.colorRGB.get()` return {r, g, b}

set RGB color

- `obj.material.colorRGB.set(r, g, b)` r,g,b from 0 to 1 

get wireframe 

-`obj.material.wireframe` return boolean 

set wireframe

-`obj.material.wireframe = boolean` boolean = true or false

get opacity

- `obj.material.opacity` return opacity from 0 to 1

set opacity

- `obj.material.opacity = 0.5`

### `obj.transform`

- `obj.transform.is` return transform control state (true or false)

- `obj.transform.on()` enables the transform control for an object

- `obj.transform.off()` disables the transform control for an object

### `obj.events`

- `obj.events.leftClick.on(callback)` define leftClick event
- `obj.events.rightClick.on(callback)` define rightClick event
- `obj.events.hover.on(onCallback, outCallback)` define hover event

ex :
```javascript
//obj - SApi Object
obj.events.leftClick.on((obj) => {
    console.log(obj)
})
```

- `obj.events.leftClick.off()` disable leftClick event
- `obj.events.rightClick.off()` disable rightClick event
- `obj.events.hover.off()` disable hover event

### `obj.delete()`

- Delete the object from scene